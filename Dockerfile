# Pulling from most stable Docker Image found contains ubuntu with jdk8
FROM openjdk:8u181-alpine
ARG VERSION
#Setting the envairomental variables
ENV VERSION=$VERSION
ENV Dspring h2
ENV JAVA_OPTS "-Dserver.port=8090 -Dspring.profiles.active=h2"
#Informing which port to be exposed for external use
EXPOSE 8090
#Creating the work directory
RUN mkdir java-app
USER nobody
#Copying the needed files from the host to the docker iamge
COPY /target/assignment-$VERSION.jar /java-app/
#Running the java app at the startup
WORKDIR /java-app 
#ENTRYPOINT java -jar $JAVA_OPTS assignment-$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)-${CI_COMMIT_SHORT_SHA}.jar
ENTRYPOINT java -jar $JAVA_OPTS assignment-$VERSION.jar

export version=export version=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)-$(git rev-parse HEAD | cut -c1-8)
echo $version
docker build -t jar-app:$version --build-arg VERSION=$version .
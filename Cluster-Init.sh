#!/bin/bash
# Install MicroK8s on Linux

sudo snap install microk8s --classic

# Check the status while Kubernetes starts

microk8s status --wait-ready

# Set Alias

alias mkctl="microk8s kubectl"

# Access the Kubernetes dashboard

microk8s dashboard-proxy

# Start Microk8s

microk8s.start

version=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)-${CI_COMMIT_SHORT_SHA}
echo $version
echo $version > version.txt
# Update application version using the maven version plugin
mvn versions:set -DnewVersion=$version
# Using mvn clean package to remove old builds, create and package new release
mvn clean package

export date=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)
export sha=$(git rev-parse HEAD | cut -c1-8)
export version=$date-$sha
export pass=123456
echo $version > version.txt
echo $version
# Update application version using the maven version plugin
mvn versions:set -DnewVersion=$version
mvn clean package
